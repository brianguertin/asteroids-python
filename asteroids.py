#!/usr/bin/env python
from __future__ import division
from sgl import display,input,gui,util
from sgl.mixer import *
from sgl.input import Key
from sgl.sprites import *
from sgl.gui import *
from sgl.util import *
from sgl.time import *
import ships,weapons,effects,enemies
from ships import *
from world import World
from enemies import Asteroid
from effects import Explosion
from random import seed,randint,uniform
from math import sin,cos,ceil,floor
import sys

seed()

util.addDataDir('data')
#print util.getDataDirs()

# Game Modes
# Classic: powerups randomly appear, dissapear on death
# Campaign: powerups are purchased using money/points
# Survival: survive against neverending asteroids for best time
# Deathmatch: players fight each other

def preload():
	for p in powerups.all:
		p.preload()
	for e in effects.all:
		e.preload()

class Player(object):
	def __init__(self, ship, num, name):
		self.number = num
		self.name = name
		self.score = 0
		self.lives = 3
		self.ship = ship
		self.activeWeapon = -1
		self.table = VBox(spacing=0)
		self.healthBar = ProgressBar(progress=1.0, size=(75,10))
		self.infoLabel = Label()
		self.lifeLabel = Label()
		self.weaponIcon = Image()
		self.ammoLabel = Label()
		hbox = HBox()
		hbox.add(Image(Texture('heal.png')), Align.CENTER)
		hbox.add(self.healthBar, Expand.BOTH | Fill.BOTH)
		self.table.add(hbox, Expand.BOTH | Fill.BOTH)
		hbox = HBox()
		hbox.add(self.weaponIcon, Align.CENTER)
		hbox.add(self.ammoLabel, Expand.BOTH | Align.CENTER_LEFT)
		hbox.add(Image(Texture('life.png')), Align.CENTER)
		hbox.add(self.lifeLabel, Align.CENTER_LEFT)
		self.table.add(hbox, Expand.BOTH | Fill.BOTH)
		self.table.add(self.infoLabel)
		ship.owner = self
		self.update()
		
	def update(self):
		self.healthBar.progress = self.ship.life / self.ship.max_life
		self.infoLabel.text = 'Score: ' + str(self.score)
		self.lifeLabel.text = str(self.lives)
		aw = self.ship.currentWeapon
		if self.activeWeapon != aw:
			self.activeWeapon == aw
			self.weaponIcon.texture = Texture(type(self.ship.weapons[aw]).icon)
		self.ammoLabel.text = str(self.ship.weapons[aw].ammo)
			
def assignKeys(ship):
	input.keyboard.assign(Key.UP, ship.go, ship.stop)
	input.keyboard.assign(Key.SPACE, ship.fire, ship.hold)
	input.keyboard.assign(Key.Z, ship.prevWeapon)
	input.keyboard.assign(Key.X, ship.nextWeapon)
	input.keyboard.assign(Key.LEFT, ship.startLeftTurn, ship.stopLeftTurn)
	input.keyboard.assign(Key.RIGHT, ship.startRightTurn, ship.stopRightTurn)
	
def assignJoystick(ship):
	if len(input.joysticks):
		input.joysticks[0].assign(ship.go, ship.stop, button=0)
		input.joysticks[0].assign(ship.fire, ship.hold, button=3)
		input.joysticks[0].assign(ship.prevWeapon, button=6)
		input.joysticks[0].assign(ship.nextWeapon, button=7)
		input.joysticks[0].assign(ship.turn, axis=0)
		input.joysticks[0].assign(ship.accelerate, axis=5)
	
class ClassicGame(object):
	def __init__(self):
		self.over = False
		self.time = 0
		self.victory = False
		self.victory_music = Sound('victory.ogg')
		self.game_music = Sound('doom.ogg')
		self.portal = None
		self.level = 1
	def __del__(self):
		self.victory_music.stop()
		self.game_music.stop()
	def setup(self):
		self.victory_music.stop()
		self.game_music.play()
		self.over = False
		self.victory = False
		self.portal = None
		world = World.Get()
		#world.__init__(display.size)
		for i in range(0,3 + int(self.level**1.5)):
			world.enemies.append(Asteroid())
		for s in world.ships:
			s.age = 0
	def update(self, ticks):
		self.time += ticks
		world = World.Get()
		if not len(world.enemies) and not self.victory:
			self.victory = True
			self.portal = Object(SpriteDef('portal.sprite'))
			self.portal.position = world.width/2, world.height/2
			#self.portal.scale = 1.0
			self.game_music.stop()
			self.victory_music.play()
		elif not len(world.ships):
			self.over = True
		if self.portal:
			self.portal.update(ticks)			
			for s,p in collide(world.ships, [self.portal]):
				self.over = True
			self.portal.render()
		if self.over and self.victory:
			self.setup()
			self.level += 1
	def isOver(self):
		return (self.over and not self.victory)
		
class DeathmatchGame(object):
	def __init__(self):
		self.over = False
		self.time = 0
		self.game_music = Sound('doom.ogg')
		self.game_music.play()
	def __del__(self):
		self.game_music.stop()
	def setup(self):
		ships = World.Get().ships
		for i in range(len(ships)):
			ships[i].team = i
	def update(self, ticks):
		world = World.Get()
		self.time += ticks
		if self.time > 1000:
			self.time -= 1000
			world.spawnPowerup((randint(0, world.width), randint(0, world.height)))
			if randint(0,20) == 30:
				world.spawnEnemy(Asteroid)
	def isOver(self):
		return False
		
class SurvivalGame(object):
	def __init__(self):
		self.over = False
		self.time = 0
		self.counter = 5000
		self.game_music = Sound('doom.ogg')
		self.game_music.play()
	def __del__(self):
		self.game_music.stop()
	def setup(self):
		pass
	def update(self, ticks):
		world = World.Get()
		self.time += ticks
		self.counter += ticks
		self.spawn_time = 5000 * (.95 ** (self.time/5000))
		if self.counter > self.spawn_time:
			self.counter -= self.spawn_time
			world.spawnEnemy(Asteroid)
		if not len(world.ships):
			self.over = True
	def isOver(self):
		return self.over
		
class CampaignGame(object):
	def __init__(self, players = 1):
		self.over = False
		self.time = 0
		self.victory = False
		self.victory_music = Sound('victory.ogg')
		self.game_music = Sound('doom.ogg')
		self.portal = None
		self.level = 1
		self.spent_points = [0] * players
	def __del__(self):
		self.victory_music.stop()
		self.game_music.stop()
	def setup(self):
		world = World.Get()
		if self.level == 1:
			for s in world.ships:
				s.owner.score = 200
		self.victory_music.stop()
		self.shop()
		self.over = False
		self.victory = False
		self.portal = None
		self.game_music.play()
		#world.__init__(display.size)
		for i in range(0,3 + int(self.level**1.5)):
			world.enemies.append(Asteroid())
		for s in world.ships:
			s.age = 0
	def update(self, ticks):
		self.time += ticks
		world = World.Get()
		world.powerups = [] # Hack to remove powerups
		if not len(world.enemies) and not self.victory:
			self.victory = True
			self.portal = Object(SpriteDef('portal.sprite'))
			self.portal.position = world.width/2, world.height/2
			#self.portal.scale = 1.0
			self.game_music.stop()
			self.victory_music.play()
		elif not len(world.ships):
			self.over = True
		if self.portal:
			self.portal.update(ticks)			
			for s,p in collide(world.ships, [self.portal]):
				self.over = True
			self.portal.render()
		if self.over and self.victory:
			self.setup()
			self.level += 1
	def isOver(self):
		return (self.over and not self.victory)
	def shop(self):
		ship = World.Get().ships[0]
		cashSound = Sound('cash.wav')
		self.done_shopping = False
		def buy_health(ign):
			if ship.owner.score >= 100 and ship.life != ship.max_life:
				ship.owner.score -= 100
				ship.life += 10
				ship.life = min(ship.max_life,ship.life)
				cashSound.play()
		def buy_life(ign):
			if ship.owner.score >= 2000 and ship.owner.lives < 9:
				ship.owner.score -= 2000
				ship.owner.lives += 1
				cashSound.play()
		def buy_machinegun_ammo(ign):
			if ship.owner.score >= 50:
				ship.owner.score -= 50
				ship.weapons[1].ammo += 300
				cashSound.play()
		def buy_flamethrower_ammo(ign):
			if ship.owner.score >= 100:
				ship.owner.score -= 100
				ship.weapons[2].ammo += 400
				cashSound.play()
		def buy_missle_ammo(ign):
			if ship.owner.score >= 500:
				ship.owner.score -= 500
				ship.weapons[3].ammo += 5
				cashSound.play()
		def exit_shop(ign):
			self.done_shopping = True
		table = Table((3,5))
		table.add(Image('heal.png'), Align.CENTER)
		table.add(Label('10 Health (100 points)'), Align.CENTER)
		table.add(Button('Buy',onPress=buy_health), Align.CENTER)
		table.add(Image('life.png'), Align.CENTER)
		table.add(Label('1 Life (2000 points)'), Align.CENTER)
		table.add(Button('Buy',onPress=buy_life), Align.CENTER)
		table.add(Image('machinegun.png'), Align.CENTER)
		table.add(Label('300 Bullets (50 points)'), Align.CENTER)
		table.add(Button('Buy',onPress=buy_machinegun_ammo), Align.CENTER)
		table.add(Image('flamethrower.png'), Align.CENTER)
		table.add(Label('400 Flames (100 points)'), Align.CENTER)
		table.add(Button('Buy',onPress=buy_flamethrower_ammo), Align.CENTER)
		table.add(Image('missle_powerup.png'), Align.CENTER)
		table.add(Label('10 Missles (500 points)'), Align.CENTER)
		table.add(Button('Buy',onPress=buy_missle_ammo), Align.CENTER)
		machinegunLabel = Label()
		flamethrowerLabel = Label()
		missleLabel = Label()
		weaponTable = Table((2,3))
		weaponTable.add(Image('machinegun.png'), Align.CENTER)
		weaponTable.add(machinegunLabel, Align.CENTER)
		weaponTable.add(Image('flamethrower.png'), Align.CENTER)
		weaponTable.add(flamethrowerLabel, Align.CENTER)
		weaponTable.add(Image('missle_powerup.png'), Align.CENTER)
		weaponTable.add(missleLabel, Align.CENTER)
		vbox = VBox()
		vbox.add(weaponTable, Align.CENTER | Expand.BOTH)
		vbox.add(table, Fill.BOTH | Expand.BOTH)
		vbox.add(Button('Done',onPress=exit_shop), Expand.BOTH | Align.CENTER)
		root = Bin()
		root.size = display.size
		root.alignment = Expand.BOTH | Align.CENTER
		root.child = vbox
		while not self.done_shopping:
			machinegunLabel.text = str(ship.weapons[1].ammo)
			flamethrowerLabel.text = str(ship.weapons[2].ammo)
			missleLabel.text = str(ship.weapons[3].ammo)
			ship.owner.update()
			root.render()
			ship.owner.table.render()
			display.update()
			input.update(root)
			
class FpsDisplay(object):
	def __init__(self):
		self.count = 0
		self.frames = 0
	def update(self, ticks):
		self.frames += 1
		self.count += ticks
		if self.count > 1000:
			self.count -= 1000
			display.setCaption('Fps: ' + str(self.frames))
			self.frames = 0
			
class HeadsUpDisplay(Bin):
	def __init__(self):
		Bin.__init__(self)
		self.timeLabel = Label()
		self.table = HBox()
		self.table.add(World.Get().players[0].table, Expand.BOTH | Align.BOTTOM_LEFT)
		self.table.add(self.timeLabel, Expand.BOTH | Align.BOTTOM)
		if len(World.Get().players) > 1:
			self.table.add(World.Get().players[1].table, Expand.BOTH | Align.BOTTOM_RIGHT)
		self.size = display.size
		self.alignment = Expand.BOTH | Fill.BOTH
		self.child = self.table
		
class App(object):
	def __init__(self):
		display.init((800,600),'--fullscreen' in sys.argv,False)
		preload()
		
	def run(self):
		def start_classic(button):
			self.startGame(ClassicGame())
		def start_survival(button):
			self.startGame(SurvivalGame())
		def start_campaign(button):
			self.startGame(CampaignGame())
		def start_deathmatch(button):
			self.startGame(DeathmatchGame())
		table = VBox()
		table.add(Button('Classic',onPress=start_classic), Fill.BOTH)
		table.add(Button('Campaign',onPress=start_campaign), Fill.BOTH)
		table.add(Button('Survival',onPress=start_survival), Fill.BOTH)
		table.add(Button('Deathmatch',onPress=start_deathmatch), Fill.BOTH)
		table.add(Button('Credits',onPress=self.credits), Fill.BOTH)
		table.add(Button('Exit',onPress=sys.exit), Fill.BOTH)
		root = Bin()
		root.size = display.size
		root.alignment = Expand.BOTH | Align.CENTER
		root.child = table
		while True:
			root.render()
			display.update()
			input.update(root)
			
	def selectShips(self, players):
		return Warbird, Cruiser
		
	def startGame(self, game):
		players = 1
		ship_types = self.selectShips(players)
		world = World(display.size)
		for i in range(players):
			ship = ship_types[i]()
			ship.position = world.width / 2, world.height / 2
			world.ships.append(ship)
			world.players.append(Player(ship, i, 'Player ' + str(i+1)))
				
		hud = HeadsUpDisplay()
		game.setup()
		
		assignKeys(world.ships[0])
		if len(world.ships) > 1:
			assignJoystick(world.ships[1])
		
		victory = False
		start_time = RunningTime()
		fps = FpsDisplay()
		ticker = Ticker()
		while not input.keyboard.pressed(Key.ESCAPE) and not game.isOver():
			ticker.update()
			world.update(ticker.ticks)
			game.update(ticker.ticks)
			fps.update(ticker.ticks)
			
			time = (RunningTime() - start_time)
			time, ms = time//1000, (time % 1000)//10
			hud.timeLabel.text = str(time//60).rjust(2,'0') + ':' + str(time % 60).rjust(2,'0') + ':' + str(ms).rjust(2,'0')
			
			world.render()
			hud.render()
			display.update()
			input.update(hud)
	def credits(self, ignore=None):
		table = VBox()
		table.add(Label('Game: Brian Guertin (Myself)'), Align.CENTER)
		table.add(Label('Game Music: Doom (id Sofware)'), Align.CENTER)
		table.add(Label('Victory Music: Final Fantasy (Square Enix)'), Align.CENTER)
		hbox = HBox()
		hbox.add(Image('cruiser.png'), Align.CENTER)
		hbox.add(Image('laser.png'), Align.CENTER)
		hbox.add(Image('missle.png'), Align.CENTER)
		hbox.add(Label('Arboris'), Align.CENTER)
		table.add(hbox, Align.CENTER)
		hbox = HBox()
		hbox.add(Image('portal_frame.png'), Align.CENTER)
		hbox.add(Label('Schreib'), Align.CENTER)
		table.add(hbox, Align.CENTER)
		hbox = HBox()
		hbox.add(Image('warbird.png'), Align.CENTER)
		hbox.add(Label('Kayos'), Align.CENTER)
		table.add(hbox, Align.CENTER)
		hbox = HBox()
		hbox.add(Image('flame.png'), Align.CENTER)
		hbox.add(Label('Cybersonik'), Align.CENTER)
		table.add(hbox, Align.CENTER)
		hbox = HBox()
		hbox.add(Image('wyvern.png'), Align.CENTER)
		hbox.add(Image('tank.png'), Align.CENTER)
		hbox.add(Image('asteroid.png'), Align.CENTER)
		hbox.add(Label('Somewhere on google (sorry!)'), Align.CENTER)
		table.add(hbox, Align.CENTER)
		table.add(Spacer((0,20)))
		table.add(Label('(Press escape to go back)'), Align.CENTER)
		root = Bin()
		root.size = display.size
		root.alignment = Expand.BOTH | Align.CENTER
		root.child = table
		while not input.keyboard.pressed(Key.ESCAPE):
			root.render()
			display.update()
			input.update(root)
			
if __name__ == '__main__':
	App().run()
