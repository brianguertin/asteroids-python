from sgl.sprites import *

class Explosion(Sprite):
	def __init__(self, pos, scale):
		Sprite.__init__(self, self.preload())
		self.position = pos
		self.scale = scale
	@staticmethod
	def preload():
		return SpriteDef('explosion.sprite')

all = [Explosion]
