import pygame
from pygame.locals import *
from pygame import joystick

def do_nothing(): pass

class Key(object):
	ESCAPE = K_ESCAPE
	UP = K_UP
	DOWN = K_DOWN
	LEFT = K_LEFT
	RIGHT = K_RIGHT
	SPACE = K_SPACE
	TAB = K_TAB
	A = K_a
	#
	X = K_x
	Y = K_y
	Z = K_z
	#LSHIFT = K_LSHIFT
	
class Event(object):
	QUIT = 0

class _ButtonInfo(object):
	def __init__(self):
		self.pressed = False
		self.binds = []
		self.release_binds = []
		
class _AxisInfo(object):
	def __init__(self):
		self.value = 0.0
		self.binds = []
		
class _EventHandler(object):
	def __init__(self):
		self._event_handlers = [[]] * 5
		
	def assign(self, event, func = do_nothing):
		self._event_handlers[event].append(func)
		
	def unassign(self, event):
		self._event_handlers[event].pop()
		
	def handleEvent(self, event):
		handlers = self._event_handlers[event]
		if len(handlers):
			handlers[-1]()
			
class _Mouse(object):
	def __init__(self):
		self.axes = []
		self.buttons = []
		for i in range(0,4):
			self.axes.append(_AxisInfo())
		for i in range(0,5):
			self.buttons.append(_ButtonInfo())
			
	def assign(self, button, down = do_nothing, up = do_nothing):
		button = self.buttons[button]
		button.binds.append(down)
		button.release_binds.append(up)
		
	def unassign(self, button):
		button = self.buttons[button]
		button.binds.pop()
		button.release_binds.pop()
		
	def handleMotion(self, position, rel):
		x, y = position
		xrel, yrel = rel
		self.axes[0].value = x
		self.axes[1].value = y
		self.axes[2].value = xrel
		self.axes[3].value = yrel
		
	def handleButtonDown(self, button):
		binds = self.buttons[button].binds
		if len(binds):
			binds[-1]()
		
	def handleButtonUp(self, button):
		binds = self.buttons[button].release_binds
		if len(binds):
			binds[-1]()
			
	def _getPosition(self):
		return self.axes[0].value, self.axes[1].value
		
	position = property(_getPosition)
			
class _Keyboard(object):
	def __init__(self):
		self.keys = []
		for i in range(0,365):
			self.keys.append(_ButtonInfo())
			
	def pressed(self, key):
		return self.keys[key].pressed
			
	def assign(self, key, down = do_nothing, up = do_nothing):
		key = self.keys[key]
		key.binds.append(down)
		key.release_binds.append(up)
		
	def unassign(self, key):
		key = self.keys[key]
		key.binds.pop()
		key.release_binds.pop()
			
	def handleKeyDown(self, key):
		info = self.keys[key]
		info.pressed = True
		if len(info.binds):
			info.binds[-1]()
		
	def handleKeyUp(self, key):
		info = self.keys[key]
		info.pressed = False
		if len(info.release_binds):
			info.release_binds[-1]()
		
class _Joystick(object):
	def __init__(self,id):
		self._joy = joystick.Joystick(id)
		self._joy.init()
		print '[Joystick] Found:', self._joy.get_name()
		self.axes = []
		self.buttons = []
		for i in range(0, self._joy.get_numaxes()):
			self.axes.append(_AxisInfo())
		for i in range(0,self._joy.get_numbuttons()):
			self.buttons.append(_ButtonInfo())
			
	def assign(self, down = do_nothing, up = do_nothing, button = None, axis = None):
		if button is not None:
			button = self.buttons[button]
			button.binds.append(down)
			button.release_binds.append(up)
		if axis is not None:
			self.axes[axis].binds.append(down)
		
	def unassign(self, button):
		button = self.buttons[button]
		button.binds.pop()
		button.release_binds.pop()
		
	def handleMotion(self, axis, value):
		info = self.axes[axis]
		if info.value != value:
			info.value = value
			if len(info.binds):
				info.binds[-1](value)
			
	def handleButtonDown(self, button):
		binds = self.buttons[button].binds
		if len(binds):
			binds[-1]()
		
	def handleButtonUp(self, button):
		binds = self.buttons[button].release_binds
		if len(binds):
			binds[-1]()

mouse = _Mouse()
keyboard = _Keyboard()
events = _EventHandler()
if not joystick.get_init(): joystick.init()
joysticks = []
for id in range(0,joystick.get_count()):
	joysticks.append(_Joystick(id))

def _updateGUI(widget):
	for event in pygame.event.get():
		if event.type == KEYDOWN:
			if not widget.handleKeyDown(event.key, 0):
				keyboard.handleKeyDown(event.key)
		elif event.type == KEYUP:
			if not widget.handleKeyUp(event.key):
				keyboard.handleKeyUp(event.key)
		elif event.type == MOUSEMOTION:
			if not widget.handleMouseMotion(event.pos, event.rel):
				mouse.handleMotion(event.pos, event.rel)
		elif event.type == MOUSEBUTTONDOWN:
			if not widget.handleMouseDown(event.pos, event.button):
				mouse.handleButtonDown(event.button)
		elif event.type == MOUSEBUTTONUP:
			if not widget.handleMouseUp(event.pos, event.button):
				mouse.handleButtonUp(event.button)
		elif event.type == JOYAXISMOTION:
			joysticks[event.joy].handleMotion(event.axis, event.value)
		elif event.type == JOYBUTTONDOWN:
			joysticks[event.joy].handleButtonDown(event.button)
		elif event.type == JOYBUTTONUP:
			joysticks[event.joy].handleButtonUp(event.button)
		elif event.type == QUIT:
			global event
			events.handleEvent(Event.QUIT)

def update(widget = None):
	if widget is not None:
		_updateGUI(widget)
		return
	for event in pygame.event.get():
		if event.type == KEYDOWN:
			keyboard.handleKeyDown(event.key)
		elif event.type == KEYUP:
			keyboard.handleKeyUp(event.key)
		elif event.type == MOUSEMOTION:
			mouse.handleMotion(event.pos, event.rel)
		elif event.type == MOUSEBUTTONDOWN:
			mouse.handleButtonDown(event.button)
		elif event.type == MOUSEBUTTONUP:
			mouse.handleButtonUp(event.button)
		elif event.type == JOYAXISMOTION:
			joysticks[event.joy].handleMotion(event.axis, event.value)
		elif event.type == JOYBUTTONDOWN:
			joysticks[event.joy].handleButtonDown(event.button)
		elif event.type == JOYBUTTONUP:
			joysticks[event.joy].handleButtonUp(event.button)
		elif event.type == QUIT:
			events.handleEvent(Event.QUIT)
