from __future__ import division
from os.path import join,dirname
from math import cos,sin,radians
from sgl import display,mixer,polygon,vector,util
from sgl.util import Resource
from sgl.image import Surface,Texture
from random import uniform

class SpriteState(object):
	def __init__(self, dir = None, resource = None):
		self.name = ''
		self.loop = True
		self.speed = 150
		self.frames = []
		self.sound = None
		if resource != None:
			self.name = resource.name
			frames = resource['frames'].split(',')
			for f in frames:
				self.frames.append(int(f)-1)
			self.loop = int(resource.probe('loop','1'))
			self.speed = int(resource.probe('speed','150'))
			sound = resource.probe('sound', None)
			if sound:
				self.sound = mixer.Sound(join(dir,sound))

class SpriteDef(object):
	def __new__(cls, filename = None):
		if isinstance(filename, SpriteDef):
			return filename
		elif isinstance(filename, basestring):
			filename = util.getFile(filename)
			self = SpriteDefManager.find(filename)
			if self:
				return self
			self = object.__new__(SpriteDef)
			SpriteDefManager.add(filename, self)
		else:
			self = object.__new__(SpriteDef)
			
		self.textures = []
		self.states = []
		self.defaultState = ''
		if filename is not None:
			resource = filename
			if isinstance(filename,basestring):
				resource = Resource(filename)
			surface = Surface(join(dirname(resource.name),resource['file']))
			surfaces = surface.slice(int(resource.probe('width','1')), int(resource.probe('height','1')))
			for s in surfaces:
				self.textures.append(Texture(s))
			states = resource['states']
			for k,v in states.items():
				self.states.append(SpriteState(dirname(resource.name),v))
			self.defaultState = resource.probe('default state', 'normal')
			print '[SpriteDef] Loaded:', resource.name
		return self

class Sprite(object):
	def __init__(self, sdef):
		self.x = 0.0
		self.y = 0.0
		self.angle = 0.0
		self.scale = 1.0
		self.color = 1.0,1.0,1.0,1.0
		self.ticks = 0
		self.frame = 0
		self.mask = [] # Collision mask
		if isinstance(sdef, Texture):
			tex = sdef
			state = SpriteState()
			state.name = 'normal'
			state.frames.append(0)
			sdef = SpriteDef()
			sdef.textures.append(tex)
			sdef.states.append(state)
			sdef.defaultState = state.name
		self.data = sdef
		self._state = None
		self.state = self.data.defaultState
		assert(self._state)
		
	def update(self, ticks):
		self.ticks += ticks
		if self.ticks > len(self._state.frames) * self._state.speed and not self._state.loop:
			self.frame = self._state.frames[-1]
		else:
			frame = self.ticks / self._state.speed
			self.frame = self._state.frames[int(frame % len(self._state.frames))]
		
	def render(self):
		display.pushMatrix()
		display.color((1.0,1.0,1.0,0.7))
		#display.drawPolygon(self.mask) # For debugging
		display.translate(self.position)
		display.rotate(self.angle)
		display.translate((-self.width/2, -self.height/2))
		display.color(self.color)
		#display.color((1.0,1.0,1.0,1.0))
		display.scale(self.scale)
		self.data.textures[self.frame].render()
		display.popMatrix()
		
	def collide(self, other):
		return polygon.simplecollide(self.mask, other.mask)
		#return polygon.collide(self.mask, other.mask)
		
	def reset(self):
		self.ticks = 0
		self.frame = self._state.frames[0]
		
	def push(self, vel):
		self.velocity = vector.add(self.velocity, vel)
		
	def transform(self, vec, rot = 0.0):
		self.position = vector.add(self.position, vec)
		self.angle += rot
		self._calculateMask()
		
	def translate(self, point):
		x, y = point
		theta = radians(-self.angle)
		rx = cos(theta)*x - sin(theta)*y
		ry = sin(theta)*x + cos(theta)*y
		return self.x + rx, self.y - ry
		
	def isPlaying(self):
		if self._state.loop:
			return True
		else:
			return (self.ticks < len(self._state.frames) * self._state.speed)
		
	def _getPosition(self):
		return self.x, self.y
		
	def _setPosition(self, pos):
		self.x, self.y = pos
		
	def _getWidth(self):
		return self.data.textures[self.frame].width * self.scale
		
	def _getHeight(self):
		return self.data.textures[self.frame].height * self.scale
		
	def _getSize(self):
		return self.width, self.height
		
	def _getState(self):
		return self._state.name
		
	def _setState(self, state):
		if not self._state or self._state.name != state:
			for i in self.data.states:
				if i.name == state:
					if self._state and self._state.sound:
						self._state.sound.stop()
					self._state = i
					if self._state.sound:
						self._state.sound.play((0,-1)[self._state.loop])
					self.reset()
					break
					
	# ?!?
	def _calculateMask(self):
		hw = self.width / 2
		hh = self.height / 2
		self.mask = [
			self.translate((-hw,-hh)),
			self.translate((hw,-hh)),
			self.translate((hw,hh)),
			self.translate((-hw,hh))
			]
				
	position = property(_getPosition, _setPosition)
	width = property(_getWidth)
	height = property(_getHeight)
	size = property(_getSize)
	state = property(_getState, _setState)
	
class Object(Sprite):
	def __init__(self, data):
		Sprite.__init__(self, data)
		self.xvel = 0.0
		self.yvel = 0.0
		self.rotation = 0.0
		self.life = 0.0
		
	def update(self, ticks):
		Sprite.update(self, ticks)
		self.x += self.xvel * ticks
		self.y += self.yvel * ticks
		self.angle += self.rotation * ticks
		self._calculateMask()
		
	def isAlive(self):
		return self.life > 0.0
		
	def _getVelocity(self):
		return self.xvel, self.yvel
		
	def _setVelocity(self, vec):
		self.xvel, self.yvel = vec
		
	velocity = property(_getVelocity, _setVelocity)
	
def collide(group1, group2):
	result = []
	for first in group1:
		for second in group2:
			if first.collide(second):
				result.append((first,second))
	return result
	
# TODO: make this a *general purpose* emitter
class Emitter(Sprite):
	class Particle(object):
		def __init__(self,position,life):
			self.x, self.y = position
			self.velocity = uniform(-0.1,0.1),uniform(-0.1,0.1)
			self.life = life
			
		def update(self, ticks):
			self.life -= ticks
			self.x, self.y = vector.add(self.position, vector.multiply(self.velocity,(ticks,ticks)))
			
		def isAlive(self):
			return self.life > 0
		
		def _getPosition(self):
			return self.x, self.y
			
		def _setPosition(self, val):
			self.x, self.y = val
			
		position = property(_getPosition, _setPosition)
			
	def __init__(self, texture, position = (0,0), frequency = 30, life = 300):
		Sprite.__init__(self, Texture('nothing.png'))
		self.texture = texture
		self.x, self.y = position
		self.frequency = frequency
		self.life = life
		self.ticks = 0
		self.particles = []
		
	def update(self, ticks):
		self.ticks += ticks
		for p in self.particles:
			p.update(ticks)
		self.particles = filter(Emitter.Particle.isAlive, self.particles)
		if self.ticks >= self.frequency:
			self.ticks -= self.frequency
			self.emit()
			
	def render(self):
		for p in self.particles:
			display.pushMatrix()
			display.translate(p.position)
			display.translate((-self.texture.width/2, -self.texture.height/2))
			display.color((1.0,1.0,1.0,p.life/self.life))
			self.texture.render()
			display.popMatrix()
			
	def emit(self):
		self.particles.append(Emitter.Particle(self.position,self.life))
		
	def reset(self):
		self.ticks = 0
		self.particles = []
		
	def _getPosition(self):
		return self.x, self.y
		
	def _setPosition(self, val):
		self.x, self.y = val
		
	def _getWidth(self):
		return self.texture.width
		
	def _getHeight(self):
		return self.texture.height
		
	position = property(_getPosition, _setPosition)
	width = property(_getWidth)
	height = property(_getHeight)
		
			
SpriteDefManager = util.ResourceManager(SpriteDef)
