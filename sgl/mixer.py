from pygame import mixer
from pygame.mixer import init,quit,stop,pause,unpause,fadeout
from sgl import util

class _Sound(mixer.Sound):
	def __init__(self, param):
		mixer.Sound.__init__(self,param)
	def isPlaying(self):
		return (self.get_num_channels() == 0)
	volume = property(mixer.Sound.get_volume, mixer.Sound.set_volume)
	length = property(mixer.Sound.get_length)

def Sound(param):
	if isinstance(param, str):
		param = util.getFile(param)
		self = SoundManager.find(param)
		if not self:
			self = _Sound(param)
			SoundManager.add(param, self)
			print '[Sound] Loaded:', param
		return self
	else:
		return _Sound(param)

SoundManager = util.ResourceManager(Sound)
