import sys,warnings

def accepts(*types):
	def check_accepts(f):
		assert len(types) == f.func_code.co_argcount
		def new_f(*args, **kwds):
			for (a, t) in zip(args, types):
				assert isinstance(a, t), \
					   "arg %r does not match %s" % (a,t)
			return f(*args, **kwds)
		new_f.func_name = f.func_name
		return new_f
	return check_accepts

def returns(rtype):
	def check_returns(f):
		def new_f(*args, **kwds):
			result = f(*args, **kwds)
			assert isinstance(result, rtype), \
				"return value %r does not match %s" % (result,rtype)
			return result
		new_f.func_name = f.func_name
		return new_f
	return check_returns

def propget(func):
	locals = sys._getframe(1).f_locals
	name = func.__name__
	prop = locals.get(name)
	if not isinstance(prop, property):
		prop = property(func, doc=func.__doc__)
	else:
		doc = prop.__doc__ or func.__doc__
		prop = property(func, prop.fset, prop.fdel, doc)
	return prop

def propset(func):
	locals = sys._getframe(1).f_locals
	name = func.__name__
	prop = locals.get(name)
	if not isinstance(prop, property):
		prop = property(None, func, doc=func.__doc__)
	else:
		doc = prop.__doc__ or func.__doc__
		prop = property(prop.fget, func, prop.fdel, doc)
	return prop

def propdel(func):
	locals = sys._getframe(1).f_locals
	name = func.__name__
	prop = locals.get(name)
	if not isinstance(prop, property):
		prop = property(None, None, func, doc=func.__doc__)
	else:
		prop = property(prop.fget, prop.fset, func, prop.__doc__)
	return prop

def deprecated(func):
	"""This is a decorator which can be used to mark functions
	as deprecated. It will result in a warning being emitted
	when the function is used."""
	def newFunc(*args, **kwargs):
		warnings.warn("Call to deprecated function %s." % func.__name__,
			category=DeprecationWarning)
		return func(*args, **kwargs)
	newFunc.__name__ = func.__name__
	newFunc.__doc__ = func.__doc__
	newFunc.__dict__.update(func.__dict__)
	return newFunc
	
def synchronized(lock):
    """ Synchronization decorator. """

    def wrap(f):
        def newFunction(*args, **kw):
            lock.acquire()
            try:
                return f(*args, **kw)
            finally:
                lock.release()
        return newFunction
    return wrap

# Example usage:
#from threading import Lock
#myLock = Lock()

#@synchronized(myLock)
#def func(*args): pass

def _update_meta (self, other):
    self.__name__ = other.__name__
    self.__doc__ = other.__doc__
    self.__dict__.update(other.__dict__)
    return self

class LateBindingProperty (property):
    def __new__(cls, fget=None, fset=None, fdel=None, doc=None):

        if fget is not None:
            def __get__(obj, objtype=None, name=fget.__name__):
                fget = getattr(obj, name)
                return fget()

            fget = _update_meta(__get__, fget)

        if fset is not None:
            def __set__(obj, value, name=fset.__name__):
                fset = getattr(obj, name)
                return fset(value)
            
            fset = _update_meta(__set__, fset)

        if fdel is not None:
            def __delete__(obj, name=fdel.__name__):
                fdel = getattr(obj, name)
                return fdel()
            
            fdel = _update_meta(__delete__, fdel)

        return property(fget, fset, fdel, doc)
