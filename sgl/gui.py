from __future__ import division
from sgl import display,input,vector
from sgl.input import Key
from sgl.image import *
from sgl.font import Font
from sgl.signals import Signal
from sgl.util import Resource
from os import path

theme = None

class Theme(object):
	def __init__(self, dir = 'sgl/default.theme'):
		class ButtonTheme(object):
			def __init__(self, dir, res):
				class ButtonState(object):
					def __init__(self, dir, res):
						self.texture = Texture(path.join(dir, res['texture']))
				self.normal = ButtonState(dir, res['normal'])
				self.active = ButtonState(dir, res['active'])
				self.pressed = ButtonState(dir, res['pressed'])
				self.focused = ButtonState(dir, res['focused'])
				self.disabled = ButtonState(dir, res['disabled'])
		res = Resource(path.join(dir,'theme.resource'))
		self.font = Font(path.join(dir,res['font']['file']),int(res['font']['size']))
		self.button = ButtonTheme(dir, res['button'])

def init(filename = None):
	global theme
	if filename is not None:
		theme = Theme(filename)
	elif theme is None:
		theme = Theme()
	
class Align:
	LEFT =				0x00000001
	RIGHT =				0x00000002
	TOP =				0x00000004
	BOTTOM = 			0x00000008
	CENTER_VERTICAL =	0x00000010
	CENTER_HORIZONTAL = 0x00000020
	JUSTIFY =			0x00000040
	CENTER = CENTER_VERTICAL | CENTER_HORIZONTAL
	CENTER_LEFT = CENTER_VERTICAL | LEFT
	CENTER_RIGHT = CENTER_VERTICAL | RIGHT
	BOTTOM_LEFT = BOTTOM | LEFT
	BOTTOM_RIGHT = BOTTOM | RIGHT
	BOTTOM_CENTER = BOTTOM | CENTER_HORIZONTAL
	TOP_LEFT = TOP | LEFT
	TOP_RIGHT = TOP | RIGHT
	TOP_CENTER = TOP | CENTER_HORIZONTAL
class Expand:
	VERTICAL =   0x00000100
	HORIZONTAL = 0x00000200
	BOTH = VERTICAL | HORIZONTAL
class Fill:
	VERTICAL =   0x00001000
	HORIZONTAL = 0x00002000
	BOTH = VERTICAL | HORIZONTAL
	
	
class Widget(object):
	def __init__(self):
		init()
		#make sure theme is loaded
		self._position = 0.0, 0.0
		self._size = 0.0, 0.0
		self._requestedSize = 0.0, 0.0
		self.sigMove = Signal()
		self.sigResize = Signal()
		self.sigSizeRequest = Signal()
		
	def render(self):pass
		
	def handleMouseMotion(self, pos, rel): pass
	def handleMouseDown(self, pos, button): pass
	def handleMouseUp(self, pos, button): pass
	def handleKeyDown(self, key, unicode): pass
	def handleKeyUp(self, key): pass
	def handleMouseEnter(self): pass
	def handleMouseLeave(self): pass
	def handleGainFocus(self): pass
	def handleLoseFocus(self): pass
	def handleResize(self): pass
		
	def contains(self, pos):
		x, y = vector.subtract(pos, self.position) # x and y relative to widget
		return (x >= 0 and y >= 0 and x <= self.width and y <= self.height)
		
	def isFocusable(self): pass
	
	def _getX(self):
		return self._position[0]
		
	def _getY(self):
		return self._position[1]
		
	def _getWidth(self):
		return self._size[0]
		
	def _getHeight(self):
		return self._size[1]
		
	def _getRequestedWidth(self):
		return self._requestedSize[0]
		
	def _getRequestedHeight(self):
		return self._requestedSize[1]
		
	def _getPosition(self):
		return self._position
		
	def _getSize(self):
		return self._size
		
	def _getRequestedSize(self):
		return self._requestedSize
		
	def _setPosition(self, pos):
		if pos != self._position:
			self._position = pos
			self.sigMove(self)
		
	def _setSize(self, size):
		if size != self._size:
			self._size = size
			self.handleResize()
			self.sigResize(self)
		
	def _setRequestedSize(self, size):
		if size != self._requestedSize:
			self._requestedSize = size
			self.sigSizeRequest(self)
		
	x = property(_getX)
	y = property(_getY)
	width = property(_getWidth)
	height = property(_getHeight)
	requestedWidth = property(_getRequestedWidth)
	requestedHeight = property(_getRequestedHeight)
	position = property(_getPosition, _setPosition)
	size = property(_getSize, _setSize)
	requestedSize = property(_getRequestedSize, _setRequestedSize)
	
class Spacer(Widget):
	def __init__(self, size=(0,0)):
		Widget.__init__(self)
		self._setRequestedSize(size)

class Image(Widget):
	def __init__(self, texture = None):
		Widget.__init__(self)
		self._texture = None
		if texture:
			if not isinstance(texture, Texture):
				texture = Texture(texture)
			self._setTexture(texture)
		
	def render(self):
		if self._texture:
			display.pushMatrix()
			display.translate(self.position)
			display.color((1.0,1.0,1.0,1.0))
			self._texture.render()
			display.popMatrix()
			
	def _getTexture(self):
		return self._texture
		
	def _setTexture(self, texture):
		self._texture = texture
		if texture is not None:
			self._setRequestedSize(texture.size)
		else:
			self._setRequestedSize((0,0))
			
	texture = property(_getTexture, _setTexture)
		
class Label(Widget):
	def __init__(self, text = ''):
		Widget.__init__(self)
		self._text = ''
		self._texture = None
		self._setText(text)
		
	def render(self):
		display.pushMatrix()
		display.translate(self.position)
		display.color((1.0,1.0,1.0,1.0))
		self._texture.render()
		display.popMatrix()
		
	def _getText(self):
		return self._text
		
	def _setText(self, text):
		self._text = text
		self._texture = Texture(Surface(theme.font.render(text,True,(255,255,255))))
		self._setRequestedSize(self._texture.size)
		
	text = property(_getText, _setText)

class Composite(Widget):
	def __init__(self, child = None, alignment = 0, margin = 0):
		Widget.__init__(self)
		self.margin = margin
		self.alignment = alignment
		self._child = child or Spacer()
		self._setChild(child)
		
	def render(self):
		display.pushMatrix()
		display.translate(self.position)
		self._child.render()
		display.popMatrix()
		
	def handleResize(self):
		newPos = [self.margin,]*2
		newSize = list(self._child.requestedSize)
		filledSpace = list(newSize)
		if self.alignment & Expand.HORIZONTAL:
			filledSpace[0] = self.width - self.margin * 2
		if self.alignment & Expand.VERTICAL:
			filledSpace[1] = self.height - self.margin * 2
		if self.alignment & Fill.HORIZONTAL:
			newSize[0] = filledSpace[0]
		elif self.alignment & Align.CENTER_HORIZONTAL:
			newPos[0] += (filledSpace[0] - newSize[0]) / 2
		elif self.alignment & Align.RIGHT:
			newPos[0] += filledSpace[0] - newSize[0]
			
		if self.alignment & Fill.VERTICAL:
			newSize[1] = filledSpace[1]
		elif self.alignment & Align.CENTER_VERTICAL:
			newPos[1] += (filledSpace[1] - newSize[1]) / 2
		elif self.alignment & Align.BOTTOM:
			newPos[1] += filledSpace[1] - newSize[1]
			
		self._child.position = newPos
		self._child.size = newSize
		
	def _handleChildSizeRequest(self, child):
		self._setRequestedSize(child._getRequestedSize())
		
	def _getChild(self):
		return self._child
		
	def _setChild(self, child):
		self._child.sigSizeRequest.disconnect(self._handleChildSizeRequest)
		self._child = child or Spacer()
		self._child.sigSizeRequest.connect(self._handleChildSizeRequest)
		self._handleChildSizeRequest(self._child)
		self.handleResize()
		
	child = property(_getChild, _setChild)
		
class Bin(Composite):
	def __init__(self):
		Composite.__init__(self)
		
	def handleMouseMotion(self, pos, rel):
		return self._child.handleMouseMotion(vector.subtract(pos,self.position), rel)
	def handleMouseDown(self, pos, button):
		return self._child.handleMouseDown(vector.subtract(pos,self.position), button)
	def handleMouseUp(self, pos, button):
		return self._child.handleMouseUp(vector.subtract(pos,self.position), button)
	def handleKeyDown(self, key, unicode):
		return self._child.handleKeyDown(key, unicode)
	def handleKeyUp(self, key):
		return self._child.handleKeyUp(key)
	def handleMouseEnter(self):
		self._child.handleMouseEnter()
	def handleMouseLeave(self):
		self._child.handleMouseLeave()
	def handleGainFocus(self):
		self._child.handleGainFocus()
	def handleLoseFocus(self):
		self._child.handleLoseFocus()
		
class Box(Widget):
	def __init__(self, tabcontrol=True):
		Widget.__init__(self)
		self.margin = 0
		self.children = []
		#self.tabcontrol = tabcontrol
		self._activeWidget = None
		self._focusedWidget = None
		
	def render(self):
		display.pushMatrix()
		display.translate(self.position)
		for child in self.children:
			child.render()
		display.popMatrix()
		
	def handleMouseMotion(self, pos, rel):
		for child in self.children:
			if child.handleMouseMotion(vector.subtract(pos,self.position), rel):
				self._setActiveWidget(child)
				return True
		else:
			if self._activeWidget:
				self._setActiveWidget(None)
			return False
			
	def handleMouseDown(self, pos, button):
		for child in self.children:
			if child.handleMouseDown(vector.subtract(pos,self.position), button):
				if child.isFocusable():
					self._setFocusedWidget(child)
				return True
			
	def handleMouseUp(self, pos, button):
		# FIXME: only send mouse up events until a widget returns true (currently causes problems)
		result = False
		for child in self.children:
			if child.handleMouseUp(vector.subtract(pos,self.position), button):
				result = True
		return result
			
	def handleKeyDown(self, key, unicode):
		#if self.tabcontrol and key == Key.TAB:
		#	print 'moving to next widget'
		if self._focusedWidget:
			self._focusedWidget.handleKeyDown(key, unicode)
			return True
			
	def handleKeyUp(self, key):
		if self._focusedWidget:
			self._focusedWidget.handleKeyUp(key)
			return True
			
	def handleMouseLeave(self):
		self._activeWidget = None
		
	def handleGainFocus(self):
		if self._focusedWidget:
			self._focusedWidget.handleGainFocus()
			
	def handleLoseFocus(self):
		if self._focusedWidget:
			self._focusedWidget.handleLoseFocus()
			
	def isFocusable(self):
		return self._focusedWidget is not None
		
	def _setActiveWidget(self, widget):
		if self._activeWidget != widget:
			if self._activeWidget:
				self._activeWidget.handleMouseLeave()
			self._activeWidget = widget
			if self._activeWidget:
				self._activeWidget.handleMouseEnter()
		
	def _setFocusedWidget(self, widget):
		if self._focusedWidget != widget:
			if self._focusedWidget:
				self._focusedWidget.handleLoseFocus()
			self._focusedWidget = widget
			if self._focusedWidget:
				self._focusedWidget.handleGainFocus()
				
class Table(Box):
	def __init__(self, cells=(1,1), spacing = 5):
		Box.__init__(self)
		self.columns = cells[0]
		self.rows = cells[1]
		self.spacing = spacing
		self.alignments = []
		
	def add(self, widget, alignment = 0):
		self.children.append(widget)
		self.alignments.append(alignment)
		widget.sigSizeRequest.connect(self._handleChildSizeRequest)
		self._handleChildSizeRequest(widget)
		self.handleResize()
		
	def remove(self, widget):
		widget.sigSizeRequest.disconnect(self._handleChildSizeRequest)
		i = self.children.index(widget)
		self.children.pop(i)
		self.alignments.pop(i)
		
	def handleResize(self):
		if len(self.children):
			# Begin Test Code 
			rowSize = [0] * self.rows
			colSize = [0] * self.columns
			rowPos = colPos = 0
			rowExpand = [False] * self.rows
			colExpand = [False] * self.columns
			i = 0
			for widget in self.children:
				reqSize = widget.requestedSize
				colSize[colPos] = max(colSize[colPos], reqSize[0])
				rowSize[rowPos] = max(rowSize[rowPos], reqSize[1])
				if self.alignments[i] & Expand.VERTICAL:
					rowExpand[rowPos] = True
				if self.alignments[i] & Expand.HORIZONTAL:
					colExpand[colPos] = True
				colPos += 1
				if colPos >= self.columns:
					colPos = 0
					rowPos += 1
				if rowPos >= self.rows:
					break # TOO MANY WIDGETS
				i += 1
			rowPos = colPos = 0
			reservedWidth = reservedHeight = 0
			expandableRows = expandableColumns = 0
			extraWidth = extraHeight = 0
			for col in range(0, self.columns):
				if colExpand[col]:
					expandableColumns += 1
				reservedWidth += colSize[col]
			for row in range(0, self.rows):
				if rowExpand[row]:
					expandableRows += 1
				reservedHeight += rowSize[row]
			if expandableColumns > 0:
				extraWidth = ((self.width - self.margin * 2 - self.spacing * (self.columns - 1)) - reservedWidth) / expandableColumns
			else:
				extraWidth = 0 # NOTE: increase starting posX?
			if expandableRows > 0:
				extraHeight = ((self.height - self.margin * 2 - self.spacing * (self.rows - 1)) - reservedHeight) / expandableRows
			else:
				extraHeight = 0 # NOTE: increase starting posY?
			cellWidth = (self.width - self.margin * 2 - self.spacing * (self.columns - 1)) / self.columns
			cellHeight = (self.height - self.margin * 2 - self.spacing * (self.rows - 1)) / self.rows
			cellX = cellY = 0
			posX = posY = self.margin
			i = 0
			for widget in self.children:
				col = cellX
				row = cellY
				cellWidth = colSize[cellX]
				cellHeight = rowSize[cellY]
				if colExpand[col]:
					cellWidth += extraWidth
				if rowExpand[row]:
					cellHeight += extraHeight
				newX = posX
				newY = posY
				newWidth = cellWidth
				newHeight = cellHeight
				if (self.alignments[i] & Fill.HORIZONTAL):
					newWidth = cellWidth
				else:
					newWidth = min(widget.requestedWidth, cellWidth)
					if self.alignments[i] & Align.CENTER_HORIZONTAL:
						newX += (cellWidth - newWidth)/2
					elif self.alignments[i] & Align.RIGHT:
						newX += cellWidth - newWidth
				if self.alignments[i] & Fill.VERTICAL:
					newHeight = cellHeight
				else:
					newHeight = min(widget.requestedHeight, cellHeight)
					if self.alignments[i] & Align.CENTER_VERTICAL:
						newY += (cellHeight - newHeight)/2
					elif self.alignments[i] & Align.BOTTOM:
						newY += cellHeight - newHeight
				widget._setPosition((newX, newY))
				widget._setSize((newWidth, newHeight))
				posX += cellWidth + self.spacing
				cellX += 1
				if cellX >= self.columns:
					posX = 0
					posY += cellHeight + self.spacing
					cellX = 0
					cellY += 1
				if cellY >= self.rows:
					break
				i += 1
		
	def _handleChildSizeRequest(self, child):
		if len(self.children):
			rowSize = [0] * self.rows
			colSize = [0] * self.columns
			rowPos = colPos = 0
			for widget in self.children:
				reqSize = widget.requestedSize
				colSize[colPos] = max(colSize[colPos], reqSize[0])
				rowSize[rowPos] = max(rowSize[rowPos], reqSize[1])
				colPos += 1
				if colPos >= self.columns:
					colPos = 0
					rowPos += 1
				if rowPos >= self.rows:
					break # TOO MANY WIDGETS
			totalWidth = self.margin * 2 - self.spacing
			totalHeight = self.margin * 2 - self.spacing
			for v in colSize:
				totalWidth += v + self.spacing
			for v in rowSize:
				totalHeight += v + self.spacing
			self._setRequestedSize((totalWidth,totalHeight))
		else:
			self._setRequestedSize((self.margin * 2,)*2)
		
			
# An abstract button is the base for anything that can be pressed/released
# NOT USED
class AbstractButton(Bin):
	def __init__(self, child = None):
		Bin.__init__(self, child)
		
	def render(self):
		display.pushMatrix()
		display.translate(self.position)
		display.color((0.5,0.5,1.0,1.0))
		display.drawRectangle((0,0) + self.size)
		self._child.render()
		display.popMatrix()
		
class Button(Composite):
	def __init__(self, text = '', icon = None, onPress = None):
		Composite.__init__(self)
		self._hbox = None
		self._icon = icon
		self._label = Label()
		self._active = False
		self._pressed = False
		self._focused = False
		self._setText(text)
		self._reset()
		self.sigPress = Signal()
		if onPress is not None:
			self.sigPress.connect(onPress)
			
	def render(self):
		display.pushMatrix()
		display.translate(self.position)
		display.color((0.5,0.5,0.5,1.0))
		#display.drawFilledRectangle((0,0) + self._label.requestedSize)
		thm = (((theme.button.normal,theme.button.focused)[self._focused],theme.button.active)[self._active], theme.button.pressed)[self._pressed]
		thm.texture.renderStretched(self.size)
		#self._label.render()
		display.popMatrix()
		display.pushMatrix()
		if self._pressed:
			display.translate((1,1))
		Composite.render(self)
		display.popMatrix()
		
	def handleMouseMotion(self, pos, rel):
		return self.contains(pos)
		
	def handleMouseDown(self, pos, button):
		if self.contains(pos):
			self._pressed = True
			return True
		
	def handleMouseUp(self, pos, button):
		self._pressed = False
		if self.contains(pos):
			self.sigPress(self)
			return True
		
	def handleMouseEnter(self):
		self._active = True
		
	def handleMouseLeave(self):
		self._active = False
		
	def handleGainFocus(self):
		self._focused = True
		
	def handleLoseFocus(self):
		self._focused = False
	
	def _reset(self):
		if self._icon is not None:
			self._hbox = Table((2,1))
			self._hbox.add(self._icon)
		else:
			self._hbox = Table((1,1))
		self._hbox.add(self._label)
		self.alignment = Expand.BOTH | Align.CENTER
		self._setChild(self._hbox)
		
	def _getText(self):
		return self._label.text
		
	def _setText(self, text):
		self._label.text = text
		
	text = property(_getText, _setText)
	
class ProgressBar(Widget):
	def __init__(self, progress = 0.0, size = (0.0,0.0)):
		Widget.__init__(self)
		self._progress = 0.0
		self.requestedSize = size
		self._setProgress(progress)
		
	def render(self):
		display.pushMatrix()
		display.translate(self.position)
		display.color((1.0,1.0,1.0,1.0))
		theme.button.active.texture.renderStretched((self.width * self._progress, self.height))
		display.drawRectangle((0,0) + self.size)
		display.popMatrix()
		
	def _getProgress(self):
		return self._progress
		
	def _setProgress(self, val):
		self._progress = max(0.0,min(val,1.0))
		
	progress = property(_getProgress, _setProgress)

class HBox(Table):
	def __init__(self, **kargs):
		Table.__init__(self, **kargs)
		
	def add(self, widget, alignment = 0):
		self.columns += 1
		Table.add(self, widget, alignment)
		
	def remove(self, widget):
		self.columns -= 1
		Table.remove(self, widget)
		
class VBox(Table):
	def __init__(self, **kargs):
		Table.__init__(self, **kargs)
		
	def add(self, widget, alignment = 0):
		self.rows += 1
		Table.add(self, widget, alignment)
		
	def remove(self, widget):
		self.rows -= 1
		Table.remove(self, widget)
