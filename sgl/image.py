import os
from OpenGL.GL import *
from OpenGL.GLU import *
import pygame, pygame.image, pygame.surface
from pygame.locals import *
from sgl import display,util

class Texture(object):
	def __new__(cls, file):
		if isinstance(file, str):
			file = util.getFile(file)
			self = TextureManager.find(file)
			if self:
				return self
			self = object.__new__(Texture)
			TextureManager.add(file, self)
		else:
			self = object.__new__(Texture)
	#def __init__(self, file):
		if isinstance(file,basestring):
			try:
				textureSurface = pygame.image.load(util.getFile(file))
				print '[Texture] Loaded:', file
			except:
				print 'Error loading texture',file
				return
		elif isinstance(file, Surface):
			textureSurface = file.data
		else:
			raise TypeError('Cannot create Texture from ' + str(file))
		
		#textureSurface.set_colorkey(colorkey, RLEACCEL)
		
		self.width = textureSurface.get_width()
		self.height = textureSurface.get_height()
		self.fragment_lists = []
		
		# OpenGL textures must be a power of two
		
		def power_of_two(num):
			value = 1
			while value < num:
				value <<= 1
			return value

		width = power_of_two(textureSurface.get_width())
		height = power_of_two(textureSurface.get_height())
		self.xmod = float(self.width)/width
		self.ymod = float(self.height)/height
		
		surface = pygame.Surface((width,height),SWSURFACE|SRCALPHA,32)
		surface.blit(textureSurface,textureSurface.get_rect())
		textureData = pygame.image.tostring(surface, 'RGBA', 0)
		
		# Generate our texture from the power_of_two surface
		self.texture = glGenTextures(1)
		glBindTexture(GL_TEXTURE_2D, self.texture)
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
		#gluBuild2DMipmaps(GL_TEXTURE_2D, 4, surface.get_width(), surface.get_height(), GL_RGBA, GL_UNSIGNED_BYTE, textureData)
		glTexImage2D( GL_TEXTURE_2D, 0, 4, surface.get_width(), surface.get_height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, textureData );
		glBindTexture(GL_TEXTURE_2D, 0)
		
		
		# Generate main display which draws the whole texture
		self.lst = glGenLists(1)
		glNewList(self.lst,GL_COMPILE)
		self.renderStretched(self.size)
		glEndList()
		return self
		
		#print 'Texture created from',file,'-',str(self.width) + 'x' + str(self.height),len(self.fragment_lists),'fragments'
		
	def __del__(self):
		pass
		#glDeleteLists(self.list,1)
		#glDeleteTextures(1,self.texture)
		#print 'Texture deleted'

	def render(self):
		glCallList(self.lst)
		
	def renderStretched(self, size):
		glBindTexture(GL_TEXTURE_2D, self.texture)
		glBegin(GL_QUADS)
		glTexCoord2f(0.0, 0.0);             glVertex2f(0.0, 0.0)	# Bottom Left Of The Texture and Quad
		glTexCoord2f(self.xmod, 0.0);       glVertex2f(size[0], 0.0)	# Bottom Right Of The Texture and Quad
		glTexCoord2f(self.xmod, self.ymod); glVertex2f(size[0],  size[1])	# Top Right Of The Texture and Quad
		glTexCoord2f(0.0, self.ymod);       glVertex2f(0.0, size[1])	# Top Left Of The Texture and Quad
		glEnd()
		glBindTexture(GL_TEXTURE_2D, 0)
		
	def renderFragment(self,x1,y1,x2,y2):
		if False:
			if x2 > self.width:
				x2 = self.width
			if y2 > self.height:
				y2 = self.height
			if x1 > x2 or y1 > y2:
				return

		tex_left = float(x1)/self.width*self.xmod
		tex_right = float(x2)/self.width*self.xmod
		tex_top = float(y1)/self.height*self.ymod
		tex_bottom = float(y2)/self.height*self.ymod
		
		glBindTexture(GL_TEXTURE_2D, self.texture)
		glBegin(GL_QUADS)
		glTexCoord2f(tex_left, tex_top); glVertex2f(0, 0)	# Bottom Left Of The Texture and Quad
		glTexCoord2f(tex_right, tex_top); glVertex2f(x2 - x1, 0)	# Bottom Right Of The Texture and Quad
		glTexCoord2f(tex_right, tex_bottom); glVertex2f(x2 - x1,  y2 - y1)	# Top Right Of The Texture and Quad
		glTexCoord2f(tex_left, tex_bottom); glVertex2f(0,  y2 - y1)	# Top Left Of The Texture and Quad
		glEnd()
		glBindTexture(GL_TEXTURE_2D, 0)
		
	def _getSize(self):
		return self.width, self.height
		
	size = property(_getSize)

class Surface(object):
	def __new__(cls, param):
		if isinstance(param, str):
			param = util.getFile(param)
			self = SurfaceManager.find(param)
			if self:
				return self
			self = object.__new__(Surface)
			SurfaceManager.add(param, self)
		else:
			self = object.__new__(Surface)
			
	#def __init__(self, param):
		if isinstance(param,pygame.surface.Surface):
			self.data = param
		elif isinstance(param,tuple):
			self.data = pygame.surface.Surface(param,SRCALPHA)
		else:
			self.data = pygame.image.load(param)
			print '[Surface] Loaded:', param
		return self
			
	def slice(self, w, h):
		surfaces = []
		if self.width % w > 0 or self.height % h > 0:
			print '[Surface] Could not slice surface evenly'
		fragment_width = self.width / w
		fragment_height = self.height / h
		for y in range(0, h):
			for x in range(0, w):
				rect = fragment_width * x, fragment_height * y, fragment_width, fragment_height
				surface = Surface(self.data.subsurface(rect))
				surfaces.append(surface)
		return surfaces
		
	def _getSize(self):
		return self.data.get_size()
		
	def _getWidth(self):
		return self.data.get_width()
		
	def _getHeight(self):
		return self.data.get_height()
		
	size = property(_getSize)
	width = property(_getWidth)
	height = property(_getHeight)

SurfaceManager = util.ResourceManager(Surface)
TextureManager = util.ResourceManager(Texture)
