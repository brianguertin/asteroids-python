__all__ = ['display','gui','image','input','mixer','polygon','sprites','time','util','vector']

try:
	import psyco
	psyco.full()
except ImportError:
	pass
