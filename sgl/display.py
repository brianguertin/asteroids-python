from OpenGL.GL import *
from OpenGL.GLU import *
import pygame, pygame.image
from pygame.locals import *
from math import pi,sin,cos

width = 640
height = 480
fullscreen = False
resizable = False
framed = True
size = (0,0)

# simple float range function apparently by Peter Williams
def frange(start, stop, n):
    L = [0.0] * n
    nm1 = n - 1
    nm1inv = 1.0 / nm1
    for i in range(n):
        L[i] = nm1inv * (start*(nm1 - i) + stop*i)
    return L

def init(tsize = None, tfullscreen = None, tresizable = None, tframed = None):
	global width, height, fullscreen, resizable, framed
	width, height = tsize or (width, height)
	fullscreen = tfullscreen or fullscreen
	resizable = tresizable or resizable
	framed = tframed or framed
	_reset()
	
def setOrtho(left, right, bottom, top):
	glMatrixMode(GL_PROJECTION)
	glLoadIdentity()
	glOrtho(left, right, bottom, top, -1, 1)
	glMatrixMode(GL_MODELVIEW)
	glLoadIdentity()
	
def setOrthoAligned():
	setOrtho(0, width, height, 0)
	translate((0.325, 0.325))
	
def setOrthoScaled(length):
	aspect = length * float(width / height)
	setOrtho(-aspect, aspect, -length, length)
	
def update():
	pygame.display.flip()
	#pygame.time.wait(1000/framerate)	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
	glLoadIdentity()
	glColor4f(1.0,1.0,1.0,1.0)
	#glScissor(0, 0, sWidth, sHeight)
	
def pushMatrix():
	glPushMatrix()
	
def popMatrix():
	glPopMatrix()
	
def translate(vec):
	glTranslate(vec[0], vec[1], 0.0)
	
def rotate(angle):
	glRotatef(angle, 0.0, 0.0, 1.0)
	
def scale(x, y = None):
	y = y or x
	glScalef(x, y, 1.0)
	
def color(color):
	glColor4f(color[0], color[1], color[2], color[3])
	
def drawLine(point1, point2):
	glBegin(GL_LINES)
	glVertex2f(point1[0], point1[0])
	glVertex2f(point2[0], point2[0])
	glEnd()
	
def drawRectangle(rect):
	x, y, w, h = rect
	glBegin(GL_LINE_LOOP)
	glVertex2f(x, y)
	glVertex2f(x + w, y)
	glVertex2f(x + w, y + h)
	glVertex2f(x, y + h)
	glEnd()
	
def drawFilledRectangle(rect):
	x, y, w, h = rect
	glBegin(GL_QUADS)
	glVertex2f(x, y)
	glVertex2f(x + w, y)
	glVertex2f(x + w, y + h)
	glVertex2f(x, y + h)
	glEnd()
	
def drawPolygon(vertices):
	glBegin(GL_LINE_LOOP)
	for v in vertices:
		glVertex2f(v[0], v[1])
	glEnd()
	
def drawFilledPolygon(vertices):
	glBegin(GL_POLYGON)
	for v in vertices:
		glVertex2f(v[0], v[1])
	glEnd()
	
def drawCircle(pos, radius, detail = 50):
	glBegin(GL_LINE_LOOP)
	for i in frange(0.0, pi*2, detail):
		glVertex2f(cos(i) * radius + pos[0], sin(i) * radius + pos[1])
	glEnd()
	
def getSize():
	return width, height
	
def setCaption(text):
	pygame.display.set_caption(text)
	
def _reset():
	pygame.init()
	flags = OPENGL | DOUBLEBUF
	if fullscreen:
		flags |= FULLSCREEN
	else:
		if resizable:
			flags |= RESIZABLE
		if not framed:
			flags |= NOFRAME
	pygame.display.set_mode((width, height), flags)
	glViewport(0, 0, width, height)
	glClearColor(0.0, 0.0, 0.0, 0.0)
	glClearDepth(1.0)
	glEnable(GL_TEXTURE_2D)
	glEnable(GL_BLEND)
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
	glColor4f(1.0,1.0,1.0,1.0)
	setOrthoAligned()
	setCaption('Sgl Application')
	global size
	size = width, height
