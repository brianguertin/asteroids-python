import pygame.time
def RunningTime():
	return pygame.time.get_ticks()

class Ticker(object):
	def __init__(self):
		self._last = RunningTime()
		self.ticks = 0
		
	def update(self):
		self.ticks = RunningTime() - self._last
		self._last += self.ticks
