import polygon
from math import sqrt

def collide_polygons(poly1, poly2):
	return polygon.collide(poly1, poly2)

def collide_circles(pos1, radius1, pos2, radius2):
	x1, y1 = pos1
	x2, y2 = pos2
	overlap = (radius1 + radius2) - sqrt((x2 - x1)**2 + (y2 - y1)**2)
	# TODO: return vector!
	return max(overlap, 0.0)
