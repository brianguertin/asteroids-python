from __future__ import with_statement
from cookbook import accepts
import sys,os,warnings

_base_dir = os.path.abspath(os.path.dirname(sys.argv[0]))
_data_dirs = [_base_dir]

def getFile(path):
	if os.path.exists(path):
		return path
	for dir in _data_dirs:
		full = os.path.join(dir,path)
		if os.path.exists(full):
			return full
	else:
		raise IOError('File not found \'' + str(path) + '\'')
	
def getDataDirs():
	return _data_dirs
	
def setDataDirs(dirs):
	global _data_dirs
	_data_dirs = []
	for dir in dirs:
		addDataDir(dir)
	
def addDataDirs(dirs):
	for dir in dirs:
		addDataDir(dir)
	
def addDataDir(dir):
	if os.path.isdir(dir):
		_data_dirs.append(dir)
	else:
		dir = os.path.join(_base_dir, dir)
		if os.path.isdir(dir):
			_data_dirs.append(dir)
		else:
			raise TypeError('Invalid data directory')
	

def sign(num):
	return (1.0,-1.0)[num < 0.0]

class Resource(dict):
	def __init__(self, filename = None):
		dict.__init__(self)
		if filename is not None:
			self.name = filename
			with open(getFile(filename)) as file:
				self.load(file)
			
	def load(self, file):
		for line in file:
			if len(line) <= 0:
				continue
			line = line.strip()
			if line == 'end':
				break
			key, sep, value = line.partition(':')
			if sep:
				self.__setitem__(key.strip(), value.strip())
			else:
				res = Resource()
				res.name = line
				res.load(file)
				self.__setitem__(line, res)
				
	def probe(self, value, default):
		try:
			return self.__getitem__(value)
		except KeyError:
			return default
			
class ResourceManager(object):
	def __init__(self, type):
		self.type = type
		self.objects = {}

	def load(self, name):
		try:
			return self.objects[name]
		except KeyError:
			object = self.objects[name] = self.type(name)
			return object
			
	def find(self, name):
		for k,v in self.objects.items():
			if k == name:
				return v
				
	def add(self, name, obj):
		self.objects[name] = obj
			
	def unload(self, name):
		del self.objects[name]
				
	def purge(self):
		self.objects = {}

# The below was taken from the internets for use in my evil schemes

# The list of symbols that are included by default in the generated
# function's environment
SAFE_SYMBOLS = ["list", "dict", "tuple", "set", "long", "float", "object",
                "bool", "callable", "True", "False", "dir",
                "frozenset", "getattr", "hasattr", "abs", "cmp", "complex",
                "divmod", "id", "pow", "round", "slice", "vars",
                "hash", "hex", "int", "isinstance", "issubclass", "len",
                "map", "filter", "max", "min", "oct", "chr", "ord", "range",
                "reduce", "repr", "str", "type", "zip", "xrange", "None",
                "Exception", "KeyboardInterrupt"]
# Also add the standard exceptions
__bi = __builtins__
if type(__bi) is not dict:
	__bi = __bi.__dict__
for k in __bi:
	if k.endswith("Error") or k.endswith("Warning"):
		SAFE_SYMBOLS.append(k)
del __bi

def createFunction(sourceCode, args="", additional_symbols=dict()):
	"""
	Create a python function from the given source code
	
	\param sourceCode A python string containing the core of the
	function. Might include the return statement (or not), definition of
	local functions, classes, etc. Indentation matters !
	
	\param args The string representing the arguments to put in the function's
	prototype, such as "a, b", or "a=12, b",
	or "a=12, b=dict(akey=42, another=5)"

	\param additional_symbols A dictionary variable name =>
	variable/funcion/object to include in the generated function's
	closure

	The sourceCode will be executed in a restricted environment,
	containing only the python builtins that are harmless (such as map,
	hasattr, etc.). To allow the function to access other modules or
	functions or objects, use the additional_symbols parameter. For
	example, to allow the source code to access the re and sys modules,
	as well as a global function F named afunction in the sourceCode and
	an object OoO named ooo in the sourceCode, specify:
		additional_symbols = dict(re=re, sys=sys, afunction=F, ooo=OoO)

	\return A python function implementing the source code. It can be
	recursive: the (internal) name of the function being defined is:
	__TheFunction__. Its docstring is the initial sourceCode string.

	Tests show that the resulting function does not have any calling
	time overhead (-3% to +3%, probably due to system preemption aleas)
	compared to normal python function calls.
	"""
	# Include the sourcecode as the code of a function __TheFunction__:
	s = "def __TheFunction__(%s):\n" % args
	s += "\t" + "\n\t".join(sourceCode.split('\n')) + "\n"

	# Byte-compilation (optional)
	byteCode = compile(s, "<string>", 'exec')	

	# Setup the local and global dictionaries of the execution
	# environment for __TheFunction__
	bis = dict() # builtins
	globs = dict()
	locs = dict()

	# Setup a standard-compatible python environment
	bis["locals"] = lambda: locs
	bis["globals"] = lambda: globs
	globs["__builtins__"] = bis
	globs["__name__"] = "SUBENV"
	globs["__doc__"] = sourceCode

	# Determine how the __builtins__ dictionary should be accessed
	if type(__builtins__) is dict:
		bi_dict = __builtins__
	else:
		bi_dict = __builtins__.__dict__

	# Include the safe symbols
	for k in SAFE_SYMBOLS:
		# try from current locals
		try:
			locs[k] = locals()[k]
			continue
		except KeyError:
			pass
		# Try from globals
		try:
			globs[k] = globals()[k]
			continue
		except KeyError:
			pass
		# Try from builtins
		try:
			bis[k] = bi_dict[k]
		except KeyError:
			# Symbol not available anywhere: silently ignored
			pass

	# Include the symbols added by the caller, in the globals dictionary
	globs.update(additional_symbols)

	# Finally execute the def __TheFunction__ statement:
	eval(byteCode, globs, locs)
	# As a result, the function is defined as the item __TheFunction__
	# in the locals dictionary
	fct = locs["__TheFunction__"]
	# Attach the function to the globals so that it can be recursive
	del locs["__TheFunction__"]
	globs["__TheFunction__"] = fct
	# Attach the actual source code to the docstring
	fct.__doc__ = sourceCode
	return fct
