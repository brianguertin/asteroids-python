from sgl.sprites import Object
from sgl.image import Texture
from sgl.mixer import Sound
from world import World
from effects import Explosion
from random import randint,uniform
from math import ceil,floor

# TODO: make harder to kill
class Asteroid(Object):
	def __init__(self,other = None):
		Object.__init__(self,Texture('asteroid.png'))
		self.velocity = uniform(-0.1, 0.1), uniform(-0.1, 0.1)
		self.angle = randint(0,360)
		self.rotation = uniform(-0.05, 0.05)
		self.max_life = 3.99
		if other is None:
			self.position = randint(0,World.Get().width), randint(0,World.Get().height)
			self.life = self.max_life
			self.scale = self.life / self.max_life
		else:
			self.x, self.y, self.life, self.scale = other.x, other.y, other.life, other.scale
		
	def damage(self, damage):
		old_life = self.life
		self.life -= damage
		if self.life < 0:
			self.life = 0
		self.scale = ceil(self.life) / self.max_life
		if self.life == 0 or floor(self.life) < floor(old_life):
			Sound('pop.ogg').play()
			World.Get().enemies.append(Asteroid(self))
			#World.Get().effects.append(Explosion(self.position, max(self.scale,1.0/self.max_life)))
