from sgl import vector
from sgl.mixer import Sound
from sgl.sprites import Object,SpriteDef
from sgl.image import Texture
from sgl.util import ResourceManager
from world import *
from math import sin,cos,radians
from random import randint,uniform

class HealingPowerup(Object):
	rating = 950
	def __init__(self,pos):
		Object.__init__(self, self.preload())
		self.position = pos
		self.velocity = uniform(-0.05, 0.05), uniform(-0.05, 0.05)
		self.life = 1
		
	def apply(self,ship):
		if ship.life < ship.max_life:
			ship.life += 10
			ship.life = min(ship.life, ship.max_life)
			ship.damage(0)
			self.life = 0
			Sound('zip.ogg').play()
			
	@staticmethod
	def preload():
		return Texture('heal.png')
		
class FreeLifePowerup(Object):
	rating = 990
	def __init__(self,pos):
		Object.__init__(self, self.preload())
		self.position = pos
		self.velocity = uniform(-0.05, 0.05), uniform(-0.05, 0.05)
		self.life = 1
		
	def apply(self,ship):
		if ship.owner.lives < 9:
			ship.owner.lives += 1
			self.life = 0
			Sound('zip.ogg').play()
		
	@staticmethod
	def preload():
		return Texture('life.png')
		
class MachineGunPowerup(Object):
	rating = 950
	def __init__(self,pos):
		Object.__init__(self, self.preload())
		self.position = pos
		self.velocity = uniform(-0.05, 0.05), uniform(-0.05, 0.05)
		self.life = 1
		
	def apply(self,ship):
		old_best = ship.getBestAvailableWeapon()
		ship.weapons[1].ammo += 300
		new_best = ship.getBestAvailableWeapon()
		if new_best > old_best and not ship.shooting:
			ship.weapons[ship.currentWeapon].deactivate(ship)
			ship.currentWeapon = new_best
		self.life = 0
		Sound('zip.ogg').play()
			
	@staticmethod
	def preload():
		return Texture('machinegun.png')
		
class FlamethrowerPowerup(Object):
	rating = 960
	def __init__(self,pos):
		Object.__init__(self, self.preload())
		self.position = pos
		self.velocity = uniform(-0.05, 0.05), uniform(-0.05, 0.05)
		self.life = 1
		
	def apply(self,ship):
		old_best = ship.getBestAvailableWeapon()
		ship.weapons[2].ammo += 200
		new_best = ship.getBestAvailableWeapon()
		if new_best > old_best and not ship.shooting:
			ship.weapons[ship.currentWeapon].deactivate(ship)
			ship.currentWeapon = new_best
		self.life = 0
		Sound('zip.ogg').play()
			
	@staticmethod
	def preload():
		return Texture('flamethrower.png')
		
class MissleLauncherPowerup(Object):
	rating = 980
	def __init__(self,pos):
		Object.__init__(self, self.preload())
		self.position = pos
		self.velocity = uniform(-0.05, 0.05), uniform(-0.05, 0.05)
		self.life = 1
		
	def apply(self,ship):
		old_best = ship.getBestAvailableWeapon()
		ship.weapons[3].ammo += 5
		new_best = ship.getBestAvailableWeapon()
		if new_best > old_best and not ship.shooting:
			ship.weapons[ship.currentWeapon].deactivate(ship)
			ship.currentWeapon = new_best
		self.life = 0
		Sound('zip.ogg').play()
		
	@staticmethod
	def preload():
		return Texture('missle_powerup.png')
		
all = [HealingPowerup, FreeLifePowerup, MachineGunPowerup, FlamethrowerPowerup, MissleLauncherPowerup]
