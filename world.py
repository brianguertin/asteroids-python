from sgl.image import Texture
from sgl.sprites import Sprite,Object,collide
from sgl import display,time
import powerups
from effects import Explosion
from random import randint,uniform

class World:
	instance = None
	safe_time = 1500
	def __init__(self, size):
		self.width, self.height = size
		self.background = Texture('space.png')
		self.players = []
		self.ships = []
		self.bullets = []
		self.enemies = []
		self.effects = []
		self.powerups = []
		World.instance = self
		
	def update(self, ticks):
		for group in [self.enemies,self.ships,self.bullets,self.effects,self.powerups]:
			for sprite in group:
				sprite.update(ticks)
				# Wrap around screen edges
				if sprite.x > self.width + sprite.width/2:
					sprite.x = -sprite.width/2
				elif sprite.x < -sprite.width/2:
					sprite.x = self.width + sprite.width/2
				if sprite.y > self.height + sprite.height/2:
					sprite.y = -sprite.height/2
				elif sprite.y < -sprite.height/2:
					sprite.y = self.height + sprite.height/2
					
		self.bullets = filter(Object.isAlive, self.bullets)
		self.effects = filter(Sprite.isPlaying, self.effects)
		for s,p in collide(self.ships, self.powerups):
			if p.isAlive():
				p.apply(s)
		for s,b in collide(self.ships, self.bullets):
			if b.owner.team != s.team and b.isAlive():
				#s.damage(b.damage)
				b.hit(s)
				self.effects.append(Explosion(b.position, 0.1))
				b.life = 0
				if not s.isAlive():
					player = s.owner
					if player.lives > 0:
						player.lives -= 1
						s.respawn((self.width / 2, self.height / 2))
		for b,a in collide(self.bullets, self.enemies):
			if b.isAlive():
				#a.damage(b.damage)
				b.hit(a)
				if a.life <= 0:
					self.spawnPowerup(a.position)
				b.life = 0
				player = b.owner.owner
				player.score += 10
		for s,a in collide(self.ships, self.enemies):
			if s.age > World.safe_time and a.isAlive():
				s.damage(pow(2.5,a.life))
				if not s.isAlive():
					player = s.owner
					if player.lives > 0:
						player.lives -= 1
						s.respawn((self.width / 2, self.height / 2))
				#s.owner.label.text = s.owner.info()
				self.effects.append(Explosion(a.position, a.scale))
				a.damage(max(a.life,s.life))
				if not a.isAlive():
					self.spawnPowerup(a.position)
					
		self.enemies = filter(Object.isAlive, self.enemies)
		self.ships = filter(Object.isAlive, self.ships)
		self.bullets = filter(Object.isAlive, self.bullets)
		self.powerups = filter(Object.isAlive, self.powerups)
		
		for p in self.players:
			p.update()
					
	def render(self):
		display.pushMatrix()
		bgw = bgh = 0
		while bgh < self.height:
			display.pushMatrix()
			while bgw < self.width:
				self.background.render()
				display.translate((self.background.width,0))
				bgw += self.background.width
			display.popMatrix()
			display.translate((0,self.background.height))
			bgh += self.background.height
			bgw = 0
		display.popMatrix()
		for group in [self.enemies,self.bullets,self.effects,self.powerups,self.ships]:
			for sprite in group:
				sprite.render()
		display.color((0.0,0.0,1.0,0.9))
		for s in self.ships:
			if s.age < World.safe_time:
				display.drawCircle(s.position, max(s.width, s.height)/2)
		
	def spawnPowerup(self, pos):
		for p in powerups.all:
			if randint(1,1000) > p.rating:
				self.powerups.append(p(pos))
				
	def spawnEnemy(self, cls, pos = None):
		a = cls()
		if randint(0,1):
			a.x = -a.width/2
			a.y = randint(0,self.height)
		else:
			a.x = randint(0,self.width)
			a.y = -a.height/2
		self.enemies.append(a)
		
	@staticmethod
	def Get():
		return World.instance
