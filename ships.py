from __future__ import division
from sgl import vector,time
from sgl.sprites import Object,SpriteDef
from sgl.image import Texture
from sgl.util import Resource, ResourceManager
import weapons
from weapons import *
from world import World
from math import sin,cos,radians
from random import randint,uniform

class Ship(Object):
	def __init__(self, sprite):
		res = Resource(sprite)
		Object.__init__(self, SpriteDef(sprite))
		self.age = 0
		self.team = 1
		self.position = 0,0
		self._velocity = 0.0
		self.angle = 0.0
		self.up_move = 0.0
		self.down_move = 0.0
		self.left_rot = 0.0
		self.right_rot = 0.0
		self.rot_speed = 0.0
		self.req_velocity = 0.0
		self.req_rotation = 0.0
		# Default Stats:
		self.max_velocity = 0.5
		self.accel = 0.001
		self.weight = 0.0
		self.boost = 2.0
		self.max_life = 50
		self.life = self.max_life
		self.turning = 0.2
		self.spin = self.turning
		self.offense = 1.0
		self.weapons = []
		for w in weapons.all:
			self.weapons.append(w())
		self.currentWeapon = 0
		self.shooting = False
		self.gunticks = 0
		#self.scale = 0.5
		#self.x = level.width/2 - self.size.x/2
		#self.y = level.height/2 - self.size.y()/2
		
	def nextWeapon(self):
		id = self.currentWeapon
		for i in range(0,len(self.weapons)):
			id += 1
			if id >= len(self.weapons):
				id = 0
			if self.weapons[id].ammo > 0:
				if self.shooting:
					self.weapons[self.currentWeapon].deactivate(self)
				self.currentWeapon = id
				break
		
	def prevWeapon(self):
		id = self.currentWeapon
		for i in range(0,len(self.weapons)):
			id -= 1
			if id < 0:
				id = len(self.weapons)-1
			if self.weapons[id].ammo > 0:
				if self.shooting:
					self.weapons[self.currentWeapon].deactivate(self)
				self.currentWeapon = id
				break
				
	def getBestAvailableWeapon(self):
		rng = range(0,len(self.weapons))
		rng.reverse()
		for i in rng:
			if self.weapons[i].ammo > 0:
				return i
				
	# These two functions are for joystick
	def accelerate(self, value):
		#print value
		#if value > -0.9:
		if value > 0.001:
			#self.req_velocity = (value+1)/2
			self.req_velocity = (value) * 100
			self.state = 'accel'
		else:
			self.req_velocity = 0.0
			self.state = 'normal'
			
	def turn(self, value):
		if abs(value) > 0.15:
			self.req_rotation = value
		else:
			self.req_rotation = 0.0
			
	def startLeftTurn(self):
		self.left_rot = 1.0
		self.req_rotation = self.right_rot - self.left_rot
	def stopLeftTurn(self):
		self.left_rot = 0.0
		self.req_rotation = self.right_rot - self.left_rot
	def startRightTurn(self):
		self.right_rot = 1.0
		self.req_rotation = self.right_rot - self.left_rot
	def stopRightTurn(self):
		self.right_rot = 0.0
		self.req_rotation = self.right_rot - self.left_rot
			
	def go(self):
		self.req_velocity = 1.0
		self.state = 'accel'
		
	def stop(self):
		self.req_velocity = 0.0
		self.state = 'normal'
		
	def boost(self,value):
		self.turbo_speed = value*2
	
	def damage(self,value):
		self.life -= value
		r, g, b, a = self.color
		g = b = self.life / self.max_life
		self.color = r, g, b, a
		
	def fire(self):
		if self.isAlive():
			self.shooting = True
			self.weapons[self.currentWeapon].activate(self)
			
	def hold(self):
		self.weapons[self.currentWeapon].deactivate(self)
		self.shooting = False
		
	def respawn(self, pos):
		if self.shooting:
			self.shooting = False
			self.weapons[self.currentWeapon].deactivate(self)
		self.age = 0
		self.life = self.max_life
		self.color = 1.0, 1.0, 1.0, 1.0
		self.position = pos
		self.weapons = []
		for w in weapons.all:
			self.weapons.append(w())
		self.currentWeapon = 0
		
	def update(self,ticks):
		self.age += ticks
		if self.req_velocity > 0.05:
			a = 180-self.angle
			self.xvel += sin(radians(a)) * self.accel * ticks
			self.yvel += cos(radians(a)) * self.accel * ticks
			self.rotation = self.req_rotation * self.turning
		else:
			self.rotation = self.req_rotation * self.spin
			self.xvel *= self.weight**ticks
			self.yvel *= self.weight**ticks
		vel = vector.length(self.velocity)
		if vel > self.max_velocity:
			self.xvel = (self.xvel / vel) * self.max_velocity
			self.yvel = (self.yvel / vel) * self.max_velocity
				
		Object.update(self,ticks)
		if self.shooting:
			self.gunticks += ticks
			if self.gunticks > self.weapons[self.currentWeapon].cooldown:
				self.gunticks -= self.weapons[self.currentWeapon].cooldown
				self.weapons[self.currentWeapon].persist(self)
		if self.weapons[self.currentWeapon].ammo <= 0:
			self.prevWeapon()

class Cruiser(Ship):
	def __init__(self):
		Ship.__init__(self, 'cruiser.sprite')
		self.max_velocity = 0.9
		self.accel = 0.0003
		self.weight = 0.999
		self.boost = 2.0
		self.max_life = 80
		self.turning = 0.3
		self.spin = self.turning
		self.offense = 1.1
		self.life = self.max_life
		
class Warbird(Ship):
	def __init__(self):
		Ship.__init__(self, 'warbird.sprite')
		self.max_velocity = 0.35
		self.accel = 0.0004
		self.weight = 0.997
		self.boost = 2.0
		self.max_life = 60
		self.turning = 0.3
		self.spin = self.turning
		self.offense = 1.0
		self.life = self.max_life
		
class Wyvern(Ship):
	def __init__(self):
		Ship.__init__(self, 'wyvern.sprite')
		self.max_velocity = 0.5
		self.accel = 0.0045
		self.weight = 0.1
		self.boost = 2.0
		self.max_life = 40
		self.turning = 0.0
		self.spin = 0.4
		self.offense = 0.8
		self.life = self.max_life
		
class Tank(Ship):
	def __init__(self):
		Ship.__init__(self, 'tank.sprite')
		self.max_velocity = 0.2
		self.accel = 0.0002
		self.weight = 0.999
		self.boost = 2.0
		self.max_life = 110
		self.turning = 0.13
		self.spin = self.turning
		self.offense = 1.9
		self.life = self.max_life

all = [Warbird, Cruiser, Wyvern, Tank]
