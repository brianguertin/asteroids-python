from __future__ import division
from sgl import vector
from sgl.sprites import Object,SpriteDef,Emitter
from sgl.image import Texture
from sgl.util import ResourceManager
from world import *
from math import sin,cos,radians
from random import randint,uniform

# TODO: stop copying and pasting the bullet class
class Bullet(Object):
	def __init__(self, ship, tex, damage = 1, life = 600, angle = None, scale = 1.0, speed = 0.6):
		Object.__init__(self,tex)
		if angle is None:
			angle = ship.angle
		self.scale = ship.scale
		self.position = ship.translate((0, ship.height/2))
		self.owner = ship
		self.xvel = sin(radians(angle)) * speed
		self.yvel = -cos(radians(angle)) * speed
		self.xvel += ship.xvel
		self.yvel += ship.yvel
		self.damage = damage * ship.offense
		self.max_life = life
		self.life = self.max_life
		self.scale *= scale
		self.angle = angle

	def update(self,ticks):
		Object.update(self,ticks)
		self.life -= ticks
		
	def hit(self,obj):
		obj.damage(self.damage)
		
class Missle(Bullet):
	def __init__(self, ship):
		Bullet.__init__(self, ship, SpriteDef('missle.sprite'), damage = 10.0, life = 800, speed = 0.05, scale = 1.5)

	def update(self,ticks):
		Bullet.update(self,ticks)
		#self.xvel *= 1.005**ticks
		#self.yvel *= 1.005**ticks
		self.xvel += sin(radians(self.angle)) * 0.0025 * ticks
		self.yvel += -cos(radians(self.angle)) * 0.0025 * ticks
		if vector.length(self.velocity) > 2.5:
			self.velocity = vector.scale(self.velocity, 2.5)
			
	def hit(self,obj):
		obj.damage(self.damage)
		World.Get().effects.append(Explosion(self.position, 0.3))
		
class Flame(Bullet):
	def __init__(self, ship):
		Bullet.__init__(self, ship, Texture('flame.png'), damage = 0.4, angle = ship.angle + uniform(-15.0, 15.0), life = 800, speed = 0.2)
		self.scale = 1.0 - (self.life / self.max_life)

	def update(self,ticks):
		Bullet.update(self,ticks)
		self.scale = 1.0 - (self.life / self.max_life)
		
	def hit(self,obj):
		obj.damage(self.damage)
		
class Pistol(object):
	name = 'Laser'
	icon = 'laser_icon.png'
	cooldown = 50
	def __init__(self):
		self.ammo = 1
		self.charge = 0
		self.emitter = Emitter(Texture('particle.png'), life=130, frequency=15)
		
	def activate(self,ship):
		pass
		
	def persist(self,ship):
		self.charge += 1
		if self.charge == 10:
			self.emitter.reset()
			World.Get().effects.append(self.emitter)
		self.emitter.position = ship.translate((0, (ship.height/2)))
		
		
	def deactivate(self,ship):
		if self.charge > 10:
			World.Get().bullets.append(Bullet(ship, Texture('laser.png'), scale=4.0, damage = 2.0, life = 1100))
			World.Get().effects.remove(self.emitter)
		else:
			World.Get().bullets.append(Bullet(ship, Texture('laser.png'), damage = 1.0, life = 600))
		self.charge = 0
		
class MachineGun(object):
	name = 'Machine Gun'
	icon = 'machinegun.png'
	cooldown = 50
	def __init__(self):
		self.ammo = 0
		
	def activate(self,ship):
		pass
		
	def persist(self,ship):
		World.Get().bullets.append(Bullet(ship, Texture('bullet.png'), damage = 0.3, life = 600, angle = ship.angle + uniform(-2.0, 2.0), scale = 0.5))
		self.ammo -= 1
		if self.ammo <= 0:
			ship.weapon = ship.getBestAvailableWeapon()
		
	def deactivate(self,ship):
		pass
		
class Flamethrower(object):
	name = 'Flamethrower'
	icon = 'flamethrower.png'
	cooldown = 10
	def __init__(self):
		self.ammo = 0
		
	def activate(self,ship):
		pass
		
	def persist(self,ship):
		World.Get().bullets.append(Flame(ship))
		self.ammo -= 1
		if self.ammo <= 0:
			ship.weapon = ship.getBestAvailableWeapon()
		
	def deactivate(self,ship):
		pass
	
class MissleLauncher(object):
	name = 'Missle Launcher'
	icon = 'missle_powerup.png'
	cooldown = 200
	def __init__(self):
		self.ammo = 0
		
	def activate(self,ship):
		World.Get().bullets.append(Missle(ship))
		self.ammo -= 1
		if self.ammo <= 0:
			ship.weapon = ship.getBestAvailableWeapon()
		
	def persist(self,ship):
		pass
		
	def deactivate(self,ship):
		pass
		
all = [Pistol, MachineGun, Flamethrower, MissleLauncher]
